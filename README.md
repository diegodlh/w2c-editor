# Web2Cit editor

[Web2Cit](https://meta.wikimedia.org/wiki/Web2Cit) is a set of tools to
collaboratively improve automatic citations in Wikipedia and other projects
relying on
[Zotero web translators](https://www.zotero.org/support/dev/translators/coding).

**Web2Cit editor** is an integrated Web2Cit configuration editor, with real-time
translation feedback support. It is currently under development and should replace
the current [Web2Cit editing](https://meta.wikimedia.org/wiki/Web2Cit/Editing)
workflow, which relies on individual file editing using a JSON editor.

This repository is mirrored on GitHub at https://github.com/web2cit/w2c-editor.

# Documentation

Usage and development documentation is available and maintained collaboratively
on-wiki at https://meta.wikimedia.org/wiki/Web2Cit/Docs/Editor.

# Issues

Please post any issues with the Web2Cit editor under the
[web2cit-editor](https://phabricator.wikimedia.org/tag/web2cit-editor/) tag
on Wikimedia's Phabricator. If unsure, you may use the
[web2cit](https://phabricator.wikimedia.org/tag/web2cit/) umbrella tag instead.

# Acknowledgements

Web2Cit has been initially developed with a
[grant](https://meta.wikimedia.org/wiki/Grants:Project/Diegodlh/Web2Cit:_Visual_Editor_for_Citoid_Web_Translators)
from the Wikimedia Foundation.

# License
Copyright (C) 2022 Diego de la Hera and contributors.

This work is released under the terms of
[GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.html) or any later version.
